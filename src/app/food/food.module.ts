import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestComponent } from './test/test.component';
import {RouterModule} from "@angular/router";

const Routes = [
  {
    path: 'test-component',
    component: TestComponent
  }
]

@NgModule({
  declarations: [
    TestComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(Routes)
  ]
})
export class FoodModule { }
